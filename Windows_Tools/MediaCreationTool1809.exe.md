Create Windows 10 installation media

To get started, you will first need to have a licence to install Windows 10. You can then download and run the media creation tool. For more information on how to use the tool, see the instructions below.