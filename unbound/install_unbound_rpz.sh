#!/usr/bin/env bash

# exit on any error
set -e

#
# root has to run the script
#
if [ `whoami` != "root" ]
    then
    echo "You need to be root to do this!"
    exit 1
fi

sudo apt-get install -yqq unbound ca-certificates dnsutils curl ftp dos2unix

# make sure we have the zone dir
if [ ! -d /etc/unbound/zones/ ]
then
    mkdir -p /etc/unbound/zones/
    chown -R unbound:unbound /etc/unbound/zones/
fi

# Do we have a local blacklist?
if [ ! -f /etc/unbound/zones/adblock.local ]
then
    touch /etc/unbound/zones/adblock.local
fi

# Check for local version of rpz.conf
if [ ! -f /etc/unbound/rpz.conf ]
then
    printf "#Settings for nxdomain zones\n\ninclude: /etc/unbound/zones/adblock.local\t# My own additions\n\ninclude: /etc/unbound/zones/rpz.db\t# The sum of all imported zones\n" > /etc/unbound/rpz.conf
else
    printf "\n\n#Settings for nxdomain zones\n\ninclude: /etc/unbound/zones/adblock.local\t# My own additions\n\ninclude: /etc/unbound/zones/rpz.db\t# The sum of all imported zones\n" >> /etc/unbound/rpz.conf
fi

# Check for local rpz zones in unbound.conf
if [ 'grep rpz.conf /etc/unbound/unbound.conf' ]
then
    printf "Already setup\n"
else
    printf "include: /etc/unbound/rpz.con\n" >> /etc/unbound/unbound.conf
fi

# Make sure we have a RPZ zone update script and ensure it have the 7 bit
# (executeble)
if [ ! -f /root/update_unbound.sh ]
then
    wget -qO /root/update_unbound.sh "https://gitlab.com/rpz-zones/toolbox/raw/master/unbound/update_unbound.sh"
    chmod +x /root/update_unbound.sh
else
    wget -qO- "https://gitlab.com/rpz-zones/toolbox/raw/master/unbound/update_unbound.sh" >> /root/update_unbound.sh
    chmod +x /root/update_unbound.sh
fi

bash /root/update_unbound.sh

# Cron line explaination
# * * * * * "command to be executed"
# - - - - -
# | | | | |
# | | | | ----- Day of week (0 - 7) (Sunday=0 or 7)
# | | | ------- Month (1 - 12)
# | | --------- Day of month (1 - 31)
# | ----------- Hour (0 - 23)
# ------------- Minute (0 - 59)

# we don't need any mail on updates
#  > /dev/null 2>&1

(crontab -l ; printf "07 * * * * bash /root/update_unbound.sh > /dev/null 2>&1\n"; printf "@reboot bash /root/update_unbound.sh > /dev/null 2>&1\n") 2>&1 | sed "s/no crontab for $(whoami)//"  | sort | uniq | crontab -

unbound-checkconf && unbound-control reload;

dig +nocookie +short @127.0.0.1 mypdns.org www.mypdns.org;

echo ""
echo "Contratulation you have unbound installed with rpz zones"
echo "You should now set you hosts to point it's dns to this machine"
echo ""
echo "This host have the following ip adresses\n"
ip a | grep inet
echo ""
echo ""
echo "Done"
exit 0