# Unbound script for hosting local RPZ responses

# Open Source Threat Intelligence And Makeshift RPZ with Unbound

> Domain Name Service Response Policy Zones (DNS RPZ) is a method that allows a nameserver administrator to overlay custom information on top of the global DNS to provide alternate responses to queries. It is currently implemented in the ISC BIND nameserver (9.8 or later). Another generic name for the DNS RPZ functionality is “DNS firewall”.

# Setting up unbound

Setup is pretty simple, just add the following line to `/var/unbound/etc/unbound.conf` within the server: section of the file:

```
include: /var/unbound/zones/rpz
```

Then setup /var/unbound/zones/rpz w/ additional includes, e.g.

```
include: /var/unbound/zones/adblock.yoyo
include: /var/unbound/zones/adblock.local
include: /var/unbound/zones/ransomware.abuse.ch
```

where

- adblock.local is my local, hand-crafted block list with AD networks that are missing in the yoyo list and which I find annoying enough to block them.
- adblock.yoyo is a list provided by [yoyo](https://pgl.yoyo.org/adservers/)
- ransomware.abuse.ch is being provided by [abuse.ch](https://abuse.ch/) and is more focused on the malicious part than yoyo.
- **mypdns.org** is the master for privacy, malware amd then adblocking

Each of the files follows the specification as descibed in the [unbound.conf(5)](http://man.openbsd.org/unbound.conf) manual page. Basically I am using `local-zones` of type `always_nxdomain`, e.g.:


```
local-zone: "domain.tld" always_nxdomain
```

–EOF