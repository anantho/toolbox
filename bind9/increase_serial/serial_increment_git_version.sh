# Modified version for bind configuration managed in a git repository, that update serial only for modified zone file:

#!/bin/bash
ZONES_PATH="$(pwd)/zones"
REPO_PATH="$(git rev-parse --show-toplevel)"
DATE=$(date +%Y%m%d)
NEEDLE="Serial"
for ZONE in $(git diff --name-only --diff-filter=AM `git merge-base origin/master HEAD` $ZONES_PATH) ; do
    curr=$(/bin/grep -e "${NEEDLE}$" ${REPO_PATH}/${ZONE} | /bin/sed -n "s/^\s*\([0-9]*\)\s*;\s*${NEEDLE}\s*/\1/p")
    if [ ${#curr} -lt ${#DATE} ]; then
      serial="${DATE}00"
    else
      prefix=${curr::-2}
      if [ "$DATE" -eq "$prefix" ]; then # same day
        num=${curr: -2} # last two digits from serial number
        num=$((10#$num + 1)) # force decimal representation, increment
        serial="${DATE}$(printf '%02d' $num )" # format for 2 digits
      else
        serial="${DATE}00" # just update date
      fi
    fi
    /bin/sed -i -e "s/^\(\s*\)[0-9]\{0,\}\(\s*;\s*${NEEDLE}\)$/\1${serial}\2/" ${REPO_PATH}/${ZONE}
    echo "${ZONE}: "
    grep "; ${NEEDLE}$" ${REPO_PATH}/${ZONE}
done
