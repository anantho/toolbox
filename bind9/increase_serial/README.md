This bash script searching for:

`YYYYMMDDxx ; Serial` Case sensitive

Source: [stackexchange.com](https://unix.stackexchange.com/a/257073) All credit goes to  [Tombart](https://unix.stackexchange.com/users/34514/tombart)


> a comment from the source:
>> [@RuiFRibeiro](https://unix.stackexchange.com/users/138261/rui-f-ribeiro) If you're versioning your configuration files with git you could replace ls -1 with something like git diff that would find only changed files. – Tombart Feb 14 '18 at 7:15
