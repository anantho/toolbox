#!/usr/bin/env bash

# exit on any failure during install
set -e

#
# root has to run the script
#
if [ `whoami` != "root" ]
    then
    echo "You need to be root to do this!"
    exit 1
fi

## add ISC's PPA as source
sudo add-apt-repository ppa:isc/bind -y

## First we ensure the system packages are up to date
sudo apt-get update
sudo apt-get dist-upgrade

## Now install named aka Bind9.14
sudo apt-get install -y bind9-dnsutils bind9-host bind9-libs bind9-utils bind9 bindgraph


## Add output of generate.sh to the config for inclusion
printf 'include "/etc/bind/named.conf.blocked";' >> /etc/bind/named.conf

## download /etc/bind/db.blocked
wget -o /etc/bind/db.blocked 'https://gitlab.com/rpz-zones/toolbox/raw/master/bind9/Bind9%20Ubuntu/db.blocked'

