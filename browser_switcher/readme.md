Launch dif­fe­rent brow­sers, based on the URL to protect you better against the privacy invasive suckers like google.

The two scripts located [here](https://gitlab.com/rpz-zones/toolbox/tree/master/browser_switcher) is designed to get you started.

The first file to save is `browser-script.sh` where you'll notice the

```bash
$HOME/Desktop/tor-browser_en-US/Browser/
```

You should make this one fits to you're location of `start-tor-browser` the rest should be rather straight forward by commenting uncommenting lines

The next you have to do is to fix the line, and make match you're real location of `browser-script.sh`

```bash
Exec=/path/to/the/browser-script.sh %u
```

Then make sure the new application is registered by running `update-desktop-database ~/.local/share/applications` and voilà: It's possible to select this as the default browser in the Default Applications settings.

![browser-by-url](/uploads/2276d573f92ca66bfad600ecde3edbb6/browser-by-url.png)