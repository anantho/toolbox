#!/bin/bash

CHROMIUM=`which chromium`
FIREFOX=`which firefox`

#Launch dif­fe­rent brow­sers, based on the URL

URL=$1

GOOGLE="^https?://.+\.google\..+/"
GSEARCH="^https?://.+\.google\..+/search"

if [[ $URL =~ $GOOGLE ]] && [[ ! $URL =~ $GSEARCH ]]; then
    $HOME/Desktop/tor-browser_en-US/Browser/start-tor-browser $URL
    # if you feel chrominum for some unsafe reason should be the browser user:
    # $CHROMIUM $URL
    # for using firefox in privacy mode
    # $FIREFOX -private $URL
else
    $FIREFOX $URL
fi